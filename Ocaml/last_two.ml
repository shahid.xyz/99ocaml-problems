let rec last_two(xs:'a list) : ('a * 'a) option =
  match xs with
  | [] -> None
  (* | [x] -> None *)
  | [_] -> None
  | [x;b] -> Some (x,b)
  | _ :: rest -> last_two rest

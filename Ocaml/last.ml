(* let last = *)
(*   print_endline "Hello world" *)

let rec last (ar: 'a list) : 'a option =
  match ar with
  | [] -> None
  | [what] -> Some what
  | _ :: rest -> last rest


(* Actual Answer *)
(* let rec last = function *)
(*   | [] -> None *)
(*   | [x] -> Some x *)
(*   | _ :: tail -> last tail *)

def last(array):
    if len(array) == 0:
        print("None")
    elif len(array) == 1:
        print (array[0])
    else:
        print(array[len(array)-1])
# last([1, 2, 3, 5, 9, 19])
# last([])
last([1])
